import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("My Application")
    //color: "black"

    Image {
        id: bg
        source: "qrc:/images/images/bg-abstract-medium.jpg"
        anchors.fill: parent
    }

    Column {
        id: buttons
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        width: parent.width/2
        height: parent.height/2
        spacing: 20
        ClickableButton{ btn_text: "Start" }
        ClickableButton{ btn_text: "High Score" }
        ClickableButton{ btn_text: "Quit" }
    }

    Logo {
        width: childrenRect.width
        height: childrenRect.height+10
        anchors.bottom: buttons.top
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
