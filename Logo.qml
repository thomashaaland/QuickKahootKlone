import QtQuick 2.15


Item {
    property int text_size: parent.width/16
    Text {
        id: logo
        text: "TaHaat"
        font.family: "Helvetica"
        padding: -1
        font.pointSize: text_size
        font.bold: true
        color: "white"
        layer.samplerName: "maskSource"
    }

    Text {
        id: logoShadow
        text: "TaHaat"
        font.family: "Helvetica"
        font.pointSize: text_size
        color: "black"
        layer.samplerName: "maskSource"
        anchors.bottom: logo.bottomPadding
        anchors.left: logo.leftPadding
    }
}
