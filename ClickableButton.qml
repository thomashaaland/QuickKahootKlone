import QtQuick 2.15

Item {
    Component {
        id: normalGradient
        Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                    GradientStop { position: 0.1; color: Qt.rgba(0.1, 0.8, 0.1, 0.9) }
                    GradientStop { position: 0.12; color: Qt.rgba(0.1, 0.7, 0.9, 0.5) }
                    GradientStop { position: 0.14; color: Qt.rgba(0.8, 0.8, 1, 1) }
                    GradientStop { position: 0.33; color: Qt.rgba(0.1, 0.9, 0.1, 0.2) }
                    GradientStop { position: 0.75; color: Qt.rgba(1, 0.8, 1, 0.3) }
                    GradientStop { position: 0.9; color: Qt.rgba(0.3, 1, 0.3, 0.9) }
                    GradientStop { position: 1; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                }
    }

    Component {
        id: hoverGradient
        Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                    GradientStop { position: 0.1; color: Qt.rgba(0.1, 0.8, 0.8, 0.9) }
                    GradientStop { position: 0.12; color: Qt.rgba(0.7, 0.9, 0.9, 0.5) }
                    GradientStop { position: 0.14; color: Qt.rgba(0.8, 1, 1, 1) }
                    GradientStop { position: 0.33; color: Qt.rgba(0.1, 0.1, 0.9, 0.2) }
                    GradientStop { position: 0.75; color: Qt.rgba(0.8, 1, 1, 0.3) }
                    GradientStop { position: 0.9; color: Qt.rgba(0.3, 1, 1, 0.9) }
                    GradientStop { position: 1; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                }    }

    Component {
        id: clickGradient
        Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                    GradientStop { position: 0.1; color: Qt.rgba(0.8, 0.8, 0.6, 0.9) }
                    GradientStop { position: 0.12; color: Qt.rgba(0.7, 0.7, 0.9, 0.5) }
                    GradientStop { position: 0.14; color: Qt.rgba(0.8, 0.8, 1, 1) }
                    GradientStop { position: 0.33; color: Qt.rgba(0.9, 0.9, 0.8, 0.2) }
                    GradientStop { position: 0.75; color: Qt.rgba(1, 0.8, 0.8, 0.3) }
                    GradientStop { position: 0.9; color: Qt.rgba(1, 1, 0.8, 0.9) }
                    GradientStop { position: 1; color: Qt.rgba(0.1, 0.1, 0.1, 0.9) }
                }    }

    property int m_width: parent.width
    property int m_height: parent.height/4
    property string btn_text: ""
    width: m_width
    height: m_height

    Rectangle {
        id: btn_fill
        anchors.fill: parent
        //color: "blue"
        gradient: normalGradient.createObject(btn_fill)
        radius: m_width/20
        border.color: "green"
        border.width: 2
        Text {
            text: btn_text
            color: "#FFFFFF"
            font.pointSize: parent.height/3
            padding: 10
        }
    }

    MouseArea {
        id: mouse_area
        anchors.fill: parent
        hoverEnabled: true
        onPressed: btn_fill.gradient = clickGradient.createObject(btn_fill)
        onReleased: btn_fill.gradient = hoverGradient.createObject(btn_fill)
        onEntered: btn_fill.gradient = hoverGradient.createObject(btn_fill)
        onExited: btn_fill.gradient = normalGradient.createObject(btn_fill)
    }
}
